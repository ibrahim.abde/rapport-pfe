\chapter{Conduite du projet}
La bonne gestion d’un projet est un des facteurs clés de sa réussite, puisqu’elle permet
à tous les acteurs de projet de mener conjointement des actions organisées selon des règles
clairement exprimées. C’est dans cette perspective, que le choix d’une méthodologie adéquate
de conduite de projet demeure une tâche délicate et contraignante. Notre choix s’est porté sur la méthodologie XP fusionnée avec le modèle de développement
incrémental. Tout au long des sections qui suivent, nous commençons par introduire les
différents modèles et processus en génie logiciel, ensuite nous présentons le modèle et méthode
suivis pour finir avec l’esprit de l’intégration continue et le processus de participation dans
le projet au sein de SG ATS.
\clearpage
 
\section{Les processus de développement logiciel}
En génie logiciel, un processus de développement, ou encore une méthodologie de
développement, est la division du travail en des phases avec des activités bien définies et
regroupées afin de permettre une meilleure planification et gestion du projet. Ils existent
différents modèles de ces processus, on distingue entre autres : Le modèle en cascade, en V,
en spiral...

Chacun des différents modèles n’est qu’une réorganisation des différentes activités
principales du développement logiciel suivantes :
\begin{itemize}
    \item Spécification
    \item Conception
    \item Implémentation
    \item Intégration
    \item Test
\end{itemize}

\subsection{Les méthodes agiles}
Les méthodes agiles sont des groupes de pratiques de projets de développement en
informatique, pouvant s’appliquer à divers types de projets. Les méthodes agiles se veulent
plus pragmatiques que les méthodes traditionnelles. Elles impliquent au maximum le
demandeur (client) et permettent une grande réactivité à ses demandes. Elles visent la
satisfaction réelle du client en priorité aux termes d’un contrat de développement.

Les méthodes agiles reposent sur une structure cycle de développement commune
itérative, incrémentale et adaptative. Une méthode agile est avant tout itérative sur la base
d’un affinement du besoin mis en oeuvre dans des fonctionnalités en cours de réalisation et
même déjà réalisées. Cet affinement, indispensable à la mise en oeuvre du concept adaptatif,
se réalise en matière de génie logiciel sous deux aspects :
\begin{itemize}
    \item Fonctionnellement, par adaptation systématique du produit aux changements du
    besoin détecté par l’utilisateur lors de la conception et la réalisation du produit.
    \item Techniquement, par remaniement régulier du code déjà produit (refactoring).
\end{itemize}

Une méthode agile est ensuite, éventuellement, incrémentale. Lorsque le projet, quel que
soit le nombre de participants, dépasse en durée une dizaine de journées en moyenne, la
production de ses fonctionnalités s’effectue en plusieurs incréments.

La notion d’adaptatif, quant à elle, nécessite au-delà d’un simple principe, la mise en
oeuvre de techniques de contrôle de l’évolution du livrable et d’une métrique formelle des
modifications, avant, après et en cours de la production. Il en découle une planification
opérationnelle élémentaire, directement visible par le biais de l’affichage.

Les méthodes agiles prônent 4 valeurs fondamentales :
\begin{itemize}
    \item L’équipe ("Les individus et leurs interactions, plus que les processus et les outils") :
    dans l’optique agile, l’équipe est bien plus importante que les outils ou les procédures
    de fonctionnement. Il est préférable d’avoir une équipe soudée et qui communique,
    composée de développeurs, plutôt qu’une équipe composée d’experts fonctionnant
    chacun de manière isolée. La communication est une notion fondamentale.
    \item L’application ("Des logiciels opérationnels, plus qu’une documentation exhaustive") :
    il est vital que l’application fonctionne. Le reste, et notamment la documentation
    technique, est une aide précieuse mais non un but en soi. Une documentation précise
    est utile comme moyen de communication. La documentation représente une charge
    de travail importante, mais peut pourtant être néfaste si elle n’est pas à jour. Il est
    préférable de commenter abondamment le code lui-même, et surtout de transférer les compétences au sein de l’équipe.

    \item La collaboration ("La collaboration avec les clients, plus que la négociation
    contractuelle") : le client doit être impliqué dans le développement. On ne peut se
    contenter de négocier un contrat au début du projet, puis de négliger les demandes du
    client. Le client doit collaborer avec l’équipe et fournir un compte rendu continu sur
    l’adéquation du logiciel avec ses attentes.
    \item L’acceptation du changement ("L’adaptation au changement, plus que le suivi d’un
    plan") : la planification initiale et la structure du logiciel doivent être flexibles afin de
    permettre l’évolution de la demande du client tout au long du projet. Les premières
    livraisons du logiciel vont souvent provoquer des demandes d’évolution.
\end{itemize}

Ces quatre valeurs se déclinent en 12 principes généraux communs à toutes les méthodes
agiles :
\begin{itemize}
    \item La plus haute priorité est de satisfaire le client en livrant rapidement et régulièrement
    des fonctionnalités à forte valeur ajoutée.
\end{itemize}
Une méthode qualifiée d’agile doit donc se composer d’un ensemble de pratiques
instrumentant le cadre décrit par les 12 principes généraux agiles et en conséquence s’inscrire
dans le respect des quatre valeurs fondamentales des méthodes agiles.
\subsection{Le modèle incrémental}
Ce modèle de cycle de vie prend en compte le fait qu’un logiciel peut être construit
étape par étape. Le logiciel est spécifié et conçu dans son ensemble. La réalisation se fait par
incréments de fonctionnalités. Chaque incrément est intégré à l’ensemble des précédents et
à chaque étape le produit est testé exploité et maintenu dans son ensemble. Ce cycle de vie
permet de prendre en compte l’analyse de risques et de faire accepter progressivement un
logiciel par les utilisateurs plutôt que de faire un changement brutal des habitudes. Ce modèle
combine les éléments du modèle en cascade avec la philosophie itérative du prototypage.

Le produit est décomposé en un nombre de composants, chacun de ces composants est
conçu et construit séparément (le résultat est appelé build), puis livré au client lorsqu’on
termine. Ceci autorise l’utilisation partielle du produit et permet d’éviter un temps long de
développement, facilitant ainsi l’introduction du produit.
\subsubsection{Indicateurs et caractéristiques}
Chaque méthode correspond à un contexte associé à son application. Le modèle
incrémental peut être utilisé quand les spécifications du système en sa totalité sont complètes
et claires. Les fonctionnalités majeures doivent être précisées pour décider par ordre de
priorité les différents incréments. D’autres indicateurs sont :

\begin{itemize}
    \item Il est nécessaire d’obtenir un prototype le plus tôt possible.
    \item Une nouvelle technologie est utilisée.
    \item Il y a quelques caractéristiques à haut risque.
\end{itemize}
    
Les avantages de ce modèle sont :
\begin{itemize}    
    \item Il est généralement plus facile à tester et déboguer que d’autres méthodes de
    développement de logiciels en raison des changements relativement petits qui sont
    faits au cours de chaque itération.
    \item Le client peut répondre aux caractéristiques et examiner le produit pour tout
    changement nécessaire.
    \item La livraison du produit initial est plus rapide avec un coût réduit.
    \item Ce modèle est plus souple face aux petits changements.
\end{itemize}

\subsection{La méthode Extreme Programming}
Le premier projet de l’eXtreme Programming (XP) a commencé Mars 6, 1996. Extreme
Programming est une parmi plusieurs méthodes agiles populaires. Elle a déjà été prouvé
être très efficace dans de nombreuses entreprises de différentes tailles à travers le monde.
L’originalité de la méthode est de pousser toutes les bonnes pratiques à l’extrême (d’où son
nom) :

\begin{itemize}
    \item puisque la revue de code est une bonne pratique, elle sera faite en permanence.
    \item puisque les tests sont utiles, ils seront faits systématiquement avant chaque mise en oeuvre.
    \item puisque la conception est importante, elle sera faite tout au long du projet.
    \item puisque la simplicité permet d’avancer plus vite, nous choisirons toujours la solution la plus simple.
    \item puisque la compréhension est importante, nous définirons et ferons évoluer ensemble des métaphores.
    \item puisque l’intégration des modifications est cruciale, nous l’effectuerons plusieurs fois par jour.
    \item puisque les besoins évoluent vite, nous ferons des cycles de développement très rapides pour nous adapter au changement.
\end{itemize}
La méthode XP est un succès parce qu’il insiste sur la satisfaction du client. Au lieu de
remettre tout ce dont vous pourriez rêver à une date éloignée dans le futur ce processus fournit
le logiciel dont vous avez besoin quand vous en avez besoin. Elle permet aux développeurs
de répondre avec confiance aux exigences changeantes des clients, même tard dans le cycle
de vie.

Cette méthode met le point sur le travail d’équipe. Les managers, les clients et les
développeurs sont tous des partenaires égaux dans une équipe en collaboration. XP met en
oeuvre un environnement simple, mais efficace permettant aux équipes de devenir hautement
productives. L’équipe s’auto-organise autour du problème pour le résoudre de la manière la
plus efficace possible.

Extreme Programming améliore un projet de logiciel en cinq points essentiels : la
communication, la simplicité, le feedback, le respect et le courage. Les programmeurs,
adoptant XP, communiquent en permanence avec leurs clients et collègues. Ils gardent leur
conception simple et propre. Ils reçoivent un feedback en testant le logiciel à partir du
premier jour. Ils livrent le système aux clients dès que possible et mettent en oeuvre les
changements suggérés. Chaque petit succès approfondit leur respect pour les contributions
uniques de chaque membre de l’équipe. Avec cette fondation ils sont en mesure de répondre
avec courage à l’évolution des exigences et de la technologie. L’aspect le plus surprenant
de l’Extreme Programming est ses règles simples. Il y a beaucoup de petits morceaux,
qui, individuellement n’ont aucun sens, mais lorsqu’ils sont combinés ensemble une image
complète peut être vue. Les règles peuvent sembler maladroites et peut-être même naïf au
début, mais sont fondées sur des valeurs et des principes solides.

\section{L’intégration continue}
Vu la maturité des méthodologies de développement logiciel et leur usage courant, les
bonnes pratiques (approuvées par l’exercice) tout au long des différentes phases du cycle
de vie d’un projet de développement logiciel sont devenues d’immense importance. Sans
ces pratiques, les tentatives d’amélioration en termes de qualité d’un logiciel à travers
les méthodologies de développement agiles peuvent échouer privant ainsi les organisations
d’augmenter la valeur ajoutée livrée aux clients. On introduira dans la suite de ce rapport
le concept de l’intégration continue et les bonnes pratiques ainsi que les principes qui y sont
associés.
\subsection{Vue d'ensemble}
L’intégration continue est définie comme étant l’ensemble des pratiques, en génie logiciel,
consistant à avoir un système complètement automatisé des builds, les tests inclus, qui
s’exécute plusieurs fois par jour durant les différentes phases de développement. Ceci permet
à chaque développeur d’intégrer son travail dans l’ensemble du livrable d’une façon fréquente
(quotidiennement), permettant ainsi de réduire le nombre des problèmes d’intégration
rencontrés. Par extension de l’idée des nightly builds où les changements dans le code
sont compilés et testés chaque nuit, l’intégration continue aide à identifier les problèmes
rapidement, et ce idéalement après chaque modification individuelle du code.

Avec l’intégration continue, un système de gestion de version doit être mis en place et
les développeurs sont encouragés à mettre à jour fréquemment le dépôt partagé du code
(central code repository). Après chaque mise-a-jour de ce dépôt, un cycle de build et tests
est automatiquement lancé dont le résultat est reporté à l’ensemble des développeurs. Ce qui
renforce la communication entre les différents membres de l’équipe et les notifie de la qualité
des changements apportés. En cas de problème (de compilation par exemple), l’erreur sera
rapidement détectée et le périmètre d’investigation est restreint en général aux changements
dernièrement apportés.

Une meilleure mise en pratique d’un système d’Intégration Continue repose sur les bonnes
pratiques suivantes :
\begin{itemize}
    \item Utiliser un système de gestion de version du code.
    \item Utiliser des environnements séparés de travail pour les développeurs.
    \item Permettre aux développeurs de faire des builds en local.
    \item Mettre-à-jour le code dans le système de gestion de version d’une façon fréquente.
    \item Automatiser les différents builds et tests.
\end{itemize}

— Utiliser un système de gestion de version du code

L’activité de développer un logiciel nécessite généralement un travail parallèle des
différents développeurs, chacun avec un livrable partiel sous forme de module qui sera intégré
dans la totalité du logiciel final. Chaque changement apporté à une partie du système peut
facilement affecter l’ensemble du produit final et donc doit être suivi afin d’identifier et fixer
des problèmes potentiels. Pour cette raison un système de gestion de version du code source
doit être mis en place pour suivre de manière stricte l’évolution du code. En plus du code
source, l’ensemble des dépendances nécessaires pour construire le logiciel doivent aussi être
suivies en termes de versions, à savoir :
\begin{itemize}
    \item  Les bibliothèques tierces (Third-party libraries).
    \item  Les schémas de la base de données.
    \item  Les scripts d’installation.   
\end{itemize}

Les développeurs devraient au moins avoir accès en mode " lecture-seule " à tous les
fichiers directement du système de gestion de version. Cette approche assure que les
développeurs travaillent avec la dernière configuration de l’environnement ainsi que les
dernières modifications de leurs collègues.

Poussée à l’extrême possible, cette pratique doit offrir une récupération automatisée de
ces fichiers à partir du système de gestion de version, pour assurer que cette opération est faite
de manière correcte. Ceci est surtout important dans le cas des équipes géographiquement
distribuées où le fait de tenir les changements bien synchronisés entre les groupes dans
des différentes localisations physiques et différents fuseaux horaires peut être une activité
stimulante si on utilise une approche basée sur un seul server partagé de fichiers.

— Utiliser des environnements séparés de travail pour les développeurs

Pour extraire le maximum des bénéfices de l’intégration continue, les organisations
doivent assurer que les développeurs gardent leur productivité malgré l’état et la stabilité du
code source. Pour accomplir cette tâche, les développeurs doivent avoir des environnements
séparés de travail avec des fonctionnalités de gestion de version avancées. Parmi les avantages
offerts par ces fonctionnalités :
– Travailler en isolation.
– Reprendre la dernière version stable du code.
– Avoir un contrôle total sur la version locale du code.
– Marquer les niveaux-clés d’avancement et ne partager que le code mature bien testé.

Une fois un nouvel état stable est atteint, les collaborateurs doivent marquer ces points
d’avancement. Après marquage de plusieurs points d’avancement, le développeur peut décider
de casser ce mur d’isolation et partager ces changements en les publiant sur le système de
gestion de version ce qui affirme et déclare implicitement aux autres collaborateurs que ces
modification ont atteint un niveau de maturité et stabilité élevé en local.

— Permettre aux développeurs de faire des builds en local

Si le but principal de l’intégration continue est d’améliorer la qualité du logiciel et réduire
le taux des bugs alors le seul point important est de permettre aux développeurs de faire des
builds en local (sur leurs machines). Pour ce faire, la récupération de la dernière version stable
des fichiers nécessaires est une opération indispensable comme déjà signalé ci-dessus. Ceci
minimise le syndrome " ça marche sur ma machine ! " et factorise la tache de configuration
qui est faite une seule fois par un des développeurs et récupérée par les autres, le tout dans
une perspective de faire un build en local avec la configuration utilisée en production et
tester afin de valider les modifications faites avant de les partager.

— Mettre-à-jour le code central d’une façon fréquente

Traditionnellement, les contributeurs dans un projet tendent à reporter le partage de leurs
codes pour ne pas affecter la stabilité du livrable récemment obtenu. Malheureusement, cette
approche peut avoir un effet négatif lors du débogage d’une large quantité de code au moment
d’investigation d’un nouveau bug.
L’intégration continue encourage les équipes à partager d’une façon fréquente les
modifications apportées afin de détecter les bugs le plus tôt possible en investiguant des
bouts de code minimaux.

— Automatiser les différents builds et tests

Idéalement, un serveur d’intégration continue récupère de façon automatique et périodique la
dernière version du code dans le système de gestion de versions utilisé et appelle les différents
scripts de builds configurés. Il est important que les résultats de ces builds soient rapportés
aux équipes de développement, typiquement à travers un serveur web affichant des pages avec
les noms des tests verts ou rouges et voire même des statistiques... Une grande importance
est accordée à la compilation, qui en cas d’échec doit être fixée dans des délais critiques.


\subsection{Au sein de SG ATS : TeamCity et Github}
L’intégration continue est largement adoptée, étant un processus de technologie clé en
génie logiciel dans le coeur de la transition vers les méthodologies agiles. Couplée avec un
système robuste de gestion de versions et les bonnes pratiques présentées précédemment, elle
offre une opportunité aux développeurs, aux équipes QA et aux autres intervenants dans les
processus de développement et test afin de construire un logiciel de qualité de plus en plus
améliorée, de réduire le travail couteux de correction des bugs et en fin de compte augmenter
la valeur ajoutée livrée aux clients.
\subsubsection{TeamCity}
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{teamcity}
    \caption{Logo TeamCity}
\end{figure}

Au sein de l’équipe R\&D, à \acrshort{ats}, on utilise TeamCity qui est un système d’intégration
continue dont le principal mérite est la facilité de mise en oeuvre, d’être très graphique,
et de contenir quelques fonctionnalités qui facilitent la vie quotidienne des développeurs.
De plus, TeamCity conjugue les avantages d’être gratuit dans sa version professionnelle, et
simple à mettre en oeuvre et à configuré. Le chapitre consacré au déploiement traitera les
caractéristiques de TeamCity en détails. Quant à la gestion de versions du code, le système
utilisé est git.
\subsubsection{Git et Github}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.4]{github}
    \caption{Logo Github}
\end{figure}
git est un logiciel de gestion de versions décentralisé open source créé par Linus
Torvalds, auteur du noyau Linux, qui permet de stocker l’ensemble des fichiers sources, en
conservant la chronologie de toutes les modifications qui ont été effectuées dessus. Il permet
notamment de retrouver les différentes versions d’un lot de fichiers connexes.

Github est un service web d’hébergement et de gestion de développement de logiciels,
utilisant git. Il est centré sur l’aspect social du développement. En plus d’offrir l’hébergement
de projets avec git, le site offre de nombreuses fonctionnalités habituellement retrouvées sur
les réseaux sociaux comme les flux, la possibilité de suivre des personnes ou des projets ainsi
que des graphes de réseaux pour les dépôts. Github offre aussi la possibilité de créer un wiki
et une page web pour chaque dépôt. Le site offre aussi un logiciel de suivi de problèmes.

L’équipe R\&D possède un serveur local de Github (édition d’entreprise) pour la bonne
gestion de ses codes sources.        

\section{Processus de participation au projet}
\subsection{Création du fork}
Afin de nous permettre de travailler sur le projet principal, un compte sur Github nous a
été créé pour pouvoir manipuler notre fork. Un fork désigne une copie d’un dépôt (répertoire).
En effet, par défaut il ne nous est pas possible de faire des modifications sur un dépôt qui
ne nous appartient pas. Par conséquent, les services ont introduit cette notion de fork qui
permet de se retrouver avec un dépôt sur lequel on aura la permission d’écriture.
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{git_workflow}
    \caption{Schéma du workflow Pull Request}
\end{figure}
La notion de pull request va de pair avec le système de fork. Une fois que nous avons
effectué les changements que nous voulons sur notre fork, nous souhaitons souvent proposer
à l’auteur original nos modifications. Nous ferons alors un pull request qui consiste tout
simplement à demander à l’auteur original d’ajouter nos modifications sur le dépôt principal.
Ce processus est géré de manière quasi-automatique par le système Github de la SG.

Ce système permet de contribuer à un dépôt auquel nous n’avons pas accès en écriture
dans un environnement sécurisé afin de garder le contrôle et l’intégrité du dépôt officiel.
Nous schématisons tout ceci dans la figure 2.7.
\subsection{Utilisation des branches}
Créer une branche signifie diverger de la ligne principale de développement et continuer
à travailler sans se préoccuper de cette ligne principale. Pour notre cas, nous avons trois
branches que nous essayons de définir dans ce qui suit.
\begin{itemize}
    \item Master Branch : Il s’agit de notre ligne principale que nous essayons de garder indépendante à nos
développements, son rôle est d’avoir un lien direct avec le dépôt central pour pouvoir
rapporter les changements qui s’y effectuent d’une manière permanente et donc n’avoir
que du code entièrement stable et testé dans cette branche.
    \item Develop Branch : Cette branche sert à contenir nos développements, elle doit être mise à jour
fréquemment depuis la branche Master pour détecter rapidement les changements sur
le repository central, qui peuvent nuire au bon fonctionnement de notre code et créer
des conflits difficiles à résoudre par la suite. Cette branche n’est pas nécessairement
toujours stable, mais quand elle le devient, elle sert à intégrer nos changements dans
le repository central en passant par le pull request.
Il est nécessaire de mentionner qu’avant chaque demande pull request, une compilation
totale pour tout le dépôt local doit être effectuée afin d’éliminer le risque de créer des
anomalies au niveau du dépôt central. En cas du succès, les modifications doivent être
poussées dans notre dépôt du fork (push to origin dans la figure 2.7).
    \item Feature Branch : Cette branche est créée généralement à partir de la branche de nos développements,
permet de continuer à travailler lors du pull request, vu que ce dernier est dépendant
de la branche à partir de laquelle il a été demandé et il contient tout changement
sur cette branche jusqu’à ce qu’il soit validé, continuer à développer sur Develop
Branch lors du pull request peut engendrer de grands dégâts au niveau du dépôt central.
\end{itemize}

Le rôle de TeamCity comme outil d’intégration continue vient pour continuer cette chaîne
de développement, dans la mesure où il permet la compilation automatique et l’exécution
des différents tests pour détecter les anomalies que peuvent engendrer un changement sur le
dépôt central.

\section{Planification}
L’ordonnancement et la planification sont des phases de première nécessité dans un projet
avant de lancer l’étude du sujet et sa réalisation. Il s’agit de prévoir comment peut se dérouler
la future réalisation.

La planification est fortement recommandée dans la gestion du projet parce qu’elle a pour
intérêt :

\begin{itemize}
    \item Identifier les objectifs.
    \item Définir les contraintes du projet.
    \item Coordonner les actions.
    \item Suivre les actions en cours.
    \item Etre au courant de l'état d'avancement du projet.
    \item Diminuer les risques  
\end{itemize}

Pour réaliser le diagramme de Gantt il a fallu découper le projet en des tâches principales
et d’estimer le temps nécessaire pour chacune. Pendant la planification de notre projet nous
avons pu ressortir les taches suivantes que nous allons détailler.

Intégration : la phase d’intégration est une phase importante pour découvrir
l’environnement de travail ainsi que les outils utilisés.

Analyse de la solutions existante : Cette tâche est éminente surtout que notre projet fait l’objet
d’une migration ceci pour comprendre son fonctionnement qui doit être repris par la nouvelle solution.

Recherche des solutions condidates : Tout projet ne peut avoir lieu sans une bonne conception.

Réalisation et migration : cette phase est consacrée à l’implémentation, la migration, l’intégration et la
validation de la nouvelle solution.

Ce diagramme de Gantt présente les taches principales de notre projet.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{gantt}
    \caption{Diagramme de Gantt}
\end{figure}

\section{Conclusion}
Un projet mal conduit est condamné à l’échec, alors pour le choix d’une méthodologie
de travail s’impose et se montre indispensable. Ce chapitre retrace toute la phase de gestion
de projet effectuée et énonce la méthode de gestion de projet adoptée et ce en répertoriant
ses principes et ses concepts propres. Un processus de travail incrémental fusionné avec la
méthode XP dans un cadre d’intégration continue nous a permis de bien construire et suivre
l’évolution de la solution souhaitée, présentée dans les prochains chapitres.